package br.ucsal.teste20191;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Assert;
import org.junit.Test;

import junit.framework.TestCase;

public class TesteFatorial extends TestCase {
	@Test
	public void testarCalculo() {
		int n[] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		int y[] = { 1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880 };
		int x = 0;
		while (x >= n.length) {
			long fatorial = Calculo.calcularFat(n[x]);

			long resultadoEsperado = y[x];

			Assert.assertEquals(resultadoEsperado, fatorial);
			x++;
		}

	}

	@Test
	public void testarEntrada() {
		ByteArrayInputStream in = new ByteArrayInputStream("45".getBytes());
		System.setIn(in);
		Calculo calculo = new Calculo();
		int resObtido = calculo.obterNumero();
		int resultadoEsperado = 45;
		Assert.assertEquals(resObtido, resultadoEsperado);

	}
	public void testarSaida() {
		
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		System.setOut(new PrintStream(out));
		Calculo calculo = new Calculo();
		int n[] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		int y[] = { 1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880 };
		int x = 0;
		while (x >= n.length) {
		Calculo.imprimirFatorial(y[x], n[x]);
		String resObtido=out.toString();
		String resultadoEsperado = "O fatorial de " + n[x] + " �: " + y[x];
		Assert.assertEquals(resObtido, resultadoEsperado);
		}
	}
}
