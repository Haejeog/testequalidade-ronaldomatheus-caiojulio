package br.ucsal.teste20191;

import java.util.Scanner;

public class Calculo {
public static Scanner sc = new Scanner(System.in);
	public static void main(String[] args) {
		Integer a;
		int res;

		a = obterNumero();

		res = calcularFat(a);

		imprimirFatorial(res, a);

	}
	public static int calcularFat(int num) {

		if (num <= 1) {
			return 1;
		}else {
		return calcularFat(num - 1) * num;
		}
	}
	public static Integer obterNumero() {
		Integer n,x = 0;
		do {
			System.out.println("Informe um nmero entre 0 e 100");
			n = sc.nextInt();
			if (n >= 0 && n <= 100) {
				x = 1;
			} else {
				System.out.println("N�mero informado fora do intervalo");
			}
		} while (x == 0);
		return n;
	}

	public static void imprimirFatorial(int res, Integer n) {
		System.out.println("O fatorial de " + n + " �: " + res);

	}

}
